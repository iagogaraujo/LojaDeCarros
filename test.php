<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbName = "LojaDeCarros";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbName);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

$sql = "SHOW TABLES";
$result = $conn->query($sql) or die("error");

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo $row['Username'];
  }
} else {
  echo "0 results";
}
$conn->close();

?>